﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UltimaScriptBuilder.DTO;

namespace UltimaScriptBuilder.Forms
{
    /// <summary>
    /// Interaction logic for AddLabelWindow.xaml
    /// </summary>
    public partial class AddLabelWindow : Window
    {
        public List<UltimaLabel> Labels { get; }

        private bool HaveNoTranslation => string.IsNullOrWhiteSpace(_txtBoxValueFr.Text) && string.IsNullOrWhiteSpace(_txtBoxValueEn.Text) && string.IsNullOrWhiteSpace(_txtBoxValueEs.Text);

        public AddLabelWindow()
        {
            InitializeComponent();
            Labels = new List<UltimaLabel>();
            _lblInfo.Content = null;
            _lblErrorOneTraduction.Visibility = Visibility.Hidden;
            _lblFieldCannotBeEmpty.Visibility = Visibility.Hidden;
        }

        private void BtnClearInfo_Click(object sender, RoutedEventArgs e)
        {
            _lblInfo.Content = null;
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrWhiteSpace(_txtBoxId.Text))
            {
                _lblFieldCannotBeEmpty.Visibility = Visibility.Visible;
                return;
            }

            if (HaveNoTranslation)
            {
                _lblErrorOneTraduction.Visibility = Visibility.Visible;
                return;
            }

            Labels.Add(new UltimaLabel
            {
                OverrideExisting = _checkOverrideExistingLabel.IsChecked.Value,
                LabelId = _txtBoxId.Text,
                ValueFr = _txtBoxValueFr.Text,
                ValueEn = _txtBoxValueEn.Text,
                ValueEs = _txtBoxValueEs.Text
            });

            _lblInfo.Content = $"{_txtBoxId.Text} ajouté avec succès !";
        }

        private void BtnClearValues_Click(object sender, RoutedEventArgs e)
        {
            _txtBoxId.Text = string.Empty;
            _txtBoxValueEn.Text = string.Empty;
            _txtBoxValueFr.Text = string.Empty;
            _txtBoxValueEs.Text = string.Empty;
            ClearMessages();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TxtBoxId_TextChanged(object sender, TextChangedEventArgs e)
        {
            ClearMessages(true);
        }

        private void TxtBoxValueFr_TextChanged(object sender, TextChangedEventArgs e)
        {
            ClearMessages(false);
        }

        private void TxtBoxValueEn_TextChanged(object sender, TextChangedEventArgs e)
        {
            ClearMessages(false);
        }

        private void TxtBoxValueEs_TextChanged(object sender, TextChangedEventArgs e)
        {
            ClearMessages(false);
        }

        private void BtnValidateIfIdExists_Click(object sender, RoutedEventArgs e)
        {
            //todo : add in database validations
        }

        private void ClearMessages(bool? isOnlyIdField = null)
        {
            _lblInfo.Content = null;

            if (isOnlyIdField.HasValue && isOnlyIdField.Value)
            {
                _lblFieldCannotBeEmpty.Visibility = Visibility.Hidden;
            }
            else if (isOnlyIdField.HasValue)
            {
                _lblErrorOneTraduction.Visibility = Visibility.Hidden;
            }
            else
            {
                _lblFieldCannotBeEmpty.Visibility = Visibility.Hidden;
                _lblErrorOneTraduction.Visibility = Visibility.Hidden;
            }
        }
    }
}
