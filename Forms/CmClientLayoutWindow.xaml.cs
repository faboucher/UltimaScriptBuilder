﻿using System.Collections.Generic;
using System.Windows;
using UltimaScriptBuilder.DTO;

namespace UltimaScriptBuilder.Forms
{
    /// <summary>
    /// Interaction logic for CmClientLayoutWindow.xaml
    /// </summary>
    public partial class CmClientLayoutWindow : Window
    {
        public List<CmClientPerso> Persos { get; }

        public CmClientLayoutWindow()
        {
            InitializeComponent();
            Persos = new List<CmClientPerso>();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnClearFields_Click(object sender, RoutedEventArgs e)
        {
            _txtBoxClientCode.Text = string.Empty;
            _txtBoxLayoutName.Text = string.Empty;
            _txtBoxLayoutVersion.Text = string.Empty;
            _txtBoxControlName.Text = string.Empty;
            _txtBoxPropName.Text = string.Empty;
            _txtBoxPropValue.Text = string.Empty;
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            var property = _txtBoxPropName.Text;
            var controlName = _txtBoxControlName.Text;

            Persos.Add(new CmClientPerso
            {
                ClientCode = _txtBoxClientCode.Text,
                LayoutName = _txtBoxLayoutName.Text,
                LayoutVersion = _txtBoxLayoutVersion.Text ?? "",
                ControlName = controlName,
                PropertyName = property,
                PropertyValue = _txtBoxPropValue.Text ?? ""
            });

            _lblInfo.Content = $"{property} added on control {controlName}";

            _txtBoxPropValue.Text = "";
            _txtBoxPropName.Text = "";
        }
    }
}
