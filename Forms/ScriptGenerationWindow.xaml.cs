﻿using Ookii.Dialogs.Wpf;
using System.Windows;
using System.Windows.Controls;
using UltimaScriptBuilder.Scripts;

namespace UltimaScriptBuilder.Forms
{
    /// <summary>
    /// Interaction logic for ScriptGenerationWindow.xaml
    /// </summary>
    public partial class ScriptGenerationWindow : Window
    {
        private readonly ScriptBuilder _scriptBuilder;

        public ScriptGenerationWindow(ScriptBuilder scriptBuilder)
        {
            InitializeComponent();
            _scriptBuilder = scriptBuilder;
            UpdateFinalTitle();
        }

        private void TxtBoxProjectCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateFinalTitle();
        }

        private void TxtBoxTaskNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateFinalTitle();
        }

        private void TxtBoxScriptName_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateFinalTitle();
        }

        private void UpdateFinalTitle()
        {
            if (_txtBoxScriptName == null || _txtBoxTaskNumber == null || _txtBoxProjectCode == null)
            {
                return;
            }

            var project = string.IsNullOrWhiteSpace(_txtBoxProjectCode?.Text) ? "" : _txtBoxProjectCode.Text;
            var taskNumber = string.IsNullOrWhiteSpace(_txtBoxTaskNumber?.Text) ? "" : _txtBoxTaskNumber.Text;
            var title = string.IsNullOrWhiteSpace(_txtBoxScriptName?.Text) ? "" : _txtBoxScriptName.Text;

            _lblFinalTitle.Content = $"{project}-{taskNumber}_{title}.sql";
            _lblInfo.Content = null;
        }

        private void CheckBAddBxPropInsertion_Checked(object sender, RoutedEventArgs e)
        {
            if (_txtBoxDescriptionBxProperties == null || _lblInfo == null)
            {
                return;
            }

            _txtBoxDescriptionBxProperties.IsEnabled = _chkBoxAddBxPropInsertion.IsChecked.Value;
            _lblInfo.Content = null;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();


            if (dialog.ShowDialog(this).GetValueOrDefault())
            {
                var selectedPath = dialog.SelectedPath;
                var writer = new ScriptWriter(_scriptBuilder);
                var fileName = _lblFinalTitle.Content.ToString();
                var addBxPropInsertion = _chkBoxAddBxPropInsertion.IsChecked.Value;
                var propDescription = addBxPropInsertion ? _txtBoxDescriptionBxProperties.Text : "";

                writer.WriteFile(selectedPath, fileName, addBxPropInsertion, propDescription);
                _lblInfo.Content = $"{fileName} créé avec succès";
            }


            //_folderBrowser.ShowNewFolderButton = true;
            //var result = _folderBrowser.ShowDialog();

            //if (result == DialogResult.OK)
            //{
                //var selectedFolderPath = _folderBrowser.SelectedPath;
                //var writer = new ScriptWriter(_scriptBuilder);
                //var fileName = _tbox_CustomTitle.Text;

                //if (_check_GiveAutomatedTitle.Checked)
                //{
                //    fileName = _tbox_AutomatedTitle.Text;
                //}

                //writer.WriteFile(selectedFolderPath, fileName, _check_AddInsertDbProperties.Checked, _tbox_BxDBDescription.Text);
                //_lbl_Status.Text = "Script generated with success";
                //_lbl_Status.Visible = true;

                //if (_check_EmptyScripts.Checked)
                //{
                //    _scriptBuilder.EmptyScripts();
                //}
        }

        private void TxtBoxDescriptionBxProperties_TextChanged(object sender, TextChangedEventArgs e)
        {
            _lblInfo.Content = null;
        }
    }
}
