﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UltimaScriptBuilder.Forms;
using UltimaScriptBuilder.Scripts;

namespace UltimaScriptBuilder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ScriptBuilder _scriptBuilder;

        public MainWindow()
        {
            InitializeComponent();
            _scriptBuilder = new ScriptBuilder();
        }

        private void BtnAddLabel_Click(object sender, RoutedEventArgs e)
        {
            var addLabelWindows = new AddLabelWindow();
            addLabelWindows.Closing += AddLabelClosing;
            addLabelWindows.Show();
        }

        private void AddLabelClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var labels = ((AddLabelWindow)sender).Labels;

            if (labels.Any())
            {
                _scriptBuilder.BuildScript(labels);
            }
        }

        private void BtnGenerateScripts_Click(object sender, RoutedEventArgs e)
        {
            var createScriptsWindows = new ScriptGenerationWindow(_scriptBuilder);
            createScriptsWindows.Closing += ScriptGenerationClosing;
            createScriptsWindows.Show();
        }

        private void ScriptGenerationClosing(object sender, System.ComponentModel.CancelEventArgs e)
        { }

        private void TxtBoxCmClientLayout_Click(object sender, RoutedEventArgs e)
        {
            var cmClientLayoutWindow = new CmClientLayoutWindow();
            cmClientLayoutWindow.Closing += CmClientLayoutClosing;
            cmClientLayoutWindow.Show();
        }

        private void CmClientLayoutClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var persos = ((CmClientLayoutWindow)sender).Persos;

            if (persos.Any())
            {
                _scriptBuilder.BuildScript(persos);
            }
        }
    }
}
