﻿using System.IO;
using System.Linq;

namespace UltimaScriptBuilder.Scripts
{
    public class ScriptWriter
    {
        private readonly ScriptBuilder _builder;

        public ScriptWriter(ScriptBuilder builder)
        {
            _builder = builder;
        }

        public void WriteFile(string folderPath, string fileName, bool withBxDatabaseInsert, string bxDbDescription = null)
        {
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            var scriptNumber = 1;

            foreach (var script in _builder.Scripts)
            {
                var filePath = Path.Combine(folderPath, fileName);

                while (File.Exists(filePath))
                {
                    var extension = Path.GetExtension(fileName);
                    fileName = fileName.Replace(extension, scriptNumber + extension);
                    filePath = Path.Combine(folderPath, fileName);
                    scriptNumber++;
                }

                var finalScript = withBxDatabaseInsert ? _builder.AddBxDatabasePropToScript(script, fileName, bxDbDescription) : script;
                File.WriteAllText(filePath, finalScript);
            }

            if (!_builder.Scripts.Any() && withBxDatabaseInsert)
            {
                var filePath = Path.Combine(folderPath, fileName);

                while (File.Exists(filePath))
                {
                    var extension = Path.GetExtension(fileName);
                    fileName = fileName.Replace(extension, scriptNumber + extension);
                    filePath = Path.Combine(folderPath, fileName);
                    scriptNumber++;
                }

                File.WriteAllText(filePath, _builder.GetBxDatabasePropScript(fileName, bxDbDescription));
            }
        }
    }
}
