﻿using System.Collections.Generic;
using System.Text;
using UltimaScriptBuilder.DTO;

namespace UltimaScriptBuilder.Scripts
{
    public class ScriptBuilder
    { 
		public List<string> Scripts { get; }

        public ScriptBuilder()
        {
            Scripts = new List<string>();
        }

		public void BuildScript(List<UltimaLabel> labelValues)
		{
			var builder = new StringBuilder();
			builder.AppendLine("DECLARE");
			builder.AppendLine("@labelId VARCHAR(100)");
			builder.AppendLine(", @labelValueFR VARCHAR(2000)");
			builder.AppendLine(", @labelValueEN VARCHAR(2000)");
			builder.AppendLine(", @labelValueES VARCHAR(2000)");
			builder.AppendLine();

			foreach (var label in labelValues)
			{
				builder.AppendLine();
				builder.AppendLine($"SET @labelId = '{label.LabelId}'");
				builder.AppendLine($"SET @labelValueFR = '{label.ValueFr.Replace("'", "''")}'");
				builder.AppendLine($"SET @labelValueEN = '{label.ValueEn.Replace("'", "''")}'");
				builder.AppendLine($"SET @labelValueES = '{label.ValueEs.Replace("'", "''")}'");
				builder.AppendLine();

				if (label.OverrideExisting)
				{
					builder.AppendLine("IF EXISTS (SELECT * FROM dadLabel WHERE dadlb_xNo = @labelId)");
					builder.AppendLine("BEGIN");
					builder.AppendLine($"	{GetUpdateLabelScript("FR", "@labelValueFR")}");
					//builder.AppendLine();
					builder.AppendLine($"	{GetUpdateLabelScript("EN", "@labelValueEN")}");
					//builder.AppendLine();
					builder.AppendLine($"	{GetUpdateLabelScript("ES", "@labelValueES")}");
					builder.AppendLine("END");
					builder.AppendLine("ELSE");
				}
				else
				{
					builder.AppendLine("IF NOT EXISTS (SELECT * FROM dadLabel WHERE dadlb_xNo = @labelId)");
				}

				builder.AppendLine("BEGIN");
				builder.AppendLine("	INSERT INTO dadLabel");
				builder.AppendLine("		(dadlb_xNo, dadlg_xNo,	dadlb_xDesc)");
				builder.AppendLine("	VALUES");
				builder.AppendLine("		(@labelId,	'FR',		@labelValueFR)");
				builder.AppendLine("		,(@labelId, 'EN',		@labelValueEN)");
				builder.AppendLine("		,(@labelId, 'ES',		@labelValueES)");
				builder.AppendLine("END");
				builder.AppendLine();
			}

			builder.AppendLine("GO");
			builder.AppendLine();

			Scripts.Add(builder.ToString());
		}

        public void BuildScript(List<CmClientPerso> clientPersos)
        {
            var builder = new StringBuilder();
            builder.AppendLine("DECLARE");
            builder.AppendLine("@clientId INT");
            builder.AppendLine(", @value VARCHAR(2000)");
            builder.AppendLine(", @layoutName VARCHAR(20)");
            builder.AppendLine(", @layoutVersion VARCHAR(20)");
            builder.AppendLine(", @controlName VARCHAR(100)");
            builder.AppendLine(", @propertyName VARCHAR(100)");
            builder.AppendLine();

            string lastClientCode = null;
            string lastLayoutName = null;
            string lastLayoutVersion = null;
            string lastControlName = null;

            foreach (var perso in clientPersos)
            {
                builder.AppendLine();

                if (string.IsNullOrWhiteSpace(lastClientCode) || lastClientCode != perso.ClientCode)
                {
                    lastClientCode = perso.ClientCode;
                    builder.AppendLine($"SELECT @clientId = cmcl_iNo FROM cmClient WHERE cmcl_xCode = '{lastClientCode}'");
                }

                if (string.IsNullOrWhiteSpace(lastLayoutName) || lastLayoutName != perso.LayoutName)
                {
                    lastLayoutName = perso.LayoutName;
                    builder.AppendLine($"SET @layoutName = '{lastLayoutName}'");
                }

                if (string.IsNullOrWhiteSpace(lastLayoutVersion) || lastLayoutVersion != perso.LayoutVersion)
                {
                    lastLayoutVersion = perso.LayoutVersion;
                    builder.AppendLine($"SET @layoutVersion = '{lastLayoutVersion}'");
                }

                if (string.IsNullOrWhiteSpace(lastControlName) || lastControlName != perso.ControlName)
                {
                    lastControlName = perso.ControlName;
                    builder.AppendLine($"SET @controlName = '{lastControlName}'");
                }

                builder.AppendLine($"SET @propertyName = '{perso.PropertyName}'");
                builder.AppendLine($"SET @value = '{perso.PropertyValue.Replace("'", "''")}'");
                builder.AppendLine();

                builder.AppendLine("IF EXISTS(SELECT* FROM cmClient_LayoutControlsProperties WHERE cmcl_iNo = @clientId AND dadly_xName = @layoutName");
                builder.AppendLine("AND dadly_xVersion = @layoutVersion AND dadly_xControlName = @controlName AND dadly_xProperty = @propertyName)");
                builder.AppendLine("BEGIN");
                builder.AppendLine("	UPDATE cmClient_LayoutControlsProperties");
                builder.AppendLine("	SET dadly_xValue = @value");
                builder.AppendLine("	WHERE cmcl_iNo = @clientId");
                builder.AppendLine("	AND dadly_xName = @layoutName");
                builder.AppendLine("	AND dadly_xVersion = @layoutVersion");
                builder.AppendLine("	AND dadly_xControlName = @controlName");
                builder.AppendLine("	AND dadly_xProperty = @propertyName");
                builder.AppendLine("END");
                builder.AppendLine("ELSE");
                builder.AppendLine("BEGIN");

                builder.AppendLine("	INSERT INTO cmClient_LayoutControlsProperties");
                builder.AppendLine("		(cmcl_iNo, dadly_xName, dadly_xVersion, dadly_xControlName, dadly_xProperty, dadly_xValue)");
                builder.AppendLine("	VALUES");
                builder.AppendLine("		(@clientId, @layoutName, @layoutVersion, @controlName, @propertyName, @value)");
                builder.AppendLine("END");
                builder.AppendLine();
            }

            builder.AppendLine("GO");
            builder.AppendLine();

            Scripts.Add(builder.ToString());
        }

        public string GetBxDatabasePropScript(string fileName, string bxDbDescription)
        {
			return AddBxDatabasePropToScript("", fileName, bxDbDescription);
        }

		public string AddBxDatabasePropToScript(string script, string fileName, string bxDbDescription)
		{
			var builder = new StringBuilder(script);
			builder.AppendLine();
			builder.AppendLine("INSERT INTO bxDatabaseProperties");
			builder.AppendLine("	(bxdp_xNo,	bxdp_xDesc,	bxdp_xValeur)");
			builder.AppendLine("VALUES");
			builder.AppendLine("	(NEWID()");
			builder.AppendLine($"	, '{fileName.Replace("'", "''")}'");
			builder.AppendLine($"	, '{bxDbDescription.Replace("'", "''")}')");
			builder.AppendLine("GO");
			builder.AppendLine();
			return builder.ToString();
		}

		private string GetUpdateLabelScript(string language, string valueVariable)
		{
			var builder = new StringBuilder();
			builder.AppendLine("UPDATE dadLabel");
			builder.AppendLine($"	SET dadlb_xDesc = {valueVariable}");
			builder.AppendLine($"	WHERE dadlg_xNo = '{language}'");
			builder.AppendLine("	AND dadlb_xNo = @labelId");
			return builder.ToString();
		}
	}
}