﻿namespace UltimaScriptBuilder.DTO
{
    public class UltimaLabel
    {
        public bool OverrideExisting { get; set; }

        public string LabelId { get; set; }

        public string ValueFr { get; set; }

        public string ValueEn { get; set; }

        public string ValueEs { get; set; }
    }
}
