﻿namespace UltimaScriptBuilder.DTO
{
    public class CmClientPerso
    {
        public string ClientCode { get; set; }

        public string LayoutName { get; set; }

        public string LayoutVersion { get; set; }

        public string ControlName { get; set; }

        public string PropertyName { get; set; }

        public string PropertyValue { get; set; }
    }
}
